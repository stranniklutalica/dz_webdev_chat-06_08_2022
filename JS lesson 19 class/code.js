let hamburger = {
  size_small: { name: "small", cost: 50, ccal: 20 },
  size_large: { name: "large", cost: 100, ccal: 40 },
  stuffing_cheese: { name: "cheese", cost: 10, ccal: 20 },
  stuffing_salad: { name: "salad", cost: 20, ccal: 5 },
  stuffing_potato: { name: "potato", cost: 15, ccal: 10 },
  topping_mayo: { name: "mayo", cost: 20, ccal: 5 },
  topping_spice: { name: "spice", cost: 15, ccal: 0 },
};

function Hamburger(size, stuffing) {
  this.size = size;
  this.stuffing = stuffing;
  this.topping = {};

  if (size === undefined || stuffing === undefined) {
    console.log("Невірні данні гамбургера");
  }
}

Hamburger.prototype.getSize = function () {
  return this.size;
};

Hamburger.prototype.getStuffing = function () {
  return this.stuffing;
};

Hamburger.prototype.getToppings = function () {
  return this.topping;
};

Hamburger.prototype.calculatePrice = function () {
  let topping_cost = 0;
  for (key in this.topping) {
    topping_cost += this.topping[key].cost;
  }
  let price = this.size.cost + this.stuffing.cost + topping_cost;
  return price;
};

Hamburger.prototype.calculateCalories = function () {
  let topping_ccal = 0;
  for (key in this.topping) {
    topping_ccal += this.topping[key].ccal;
  }
  let ccal = this.size.ccal + this.stuffing.ccal + topping_ccal;
  return ccal;
};

Hamburger.prototype.addTopping = function (topping) {
  this.topping_name = topping.name;
  if (this.topping[topping.name] === undefined) {
    this.topping[topping.name] = topping;
  } else {
    console.log("Топінг вже було додано");
  }
  let toppingArr = ["small", "large", "cheese", "salad", "potato"];

  for (let i = 0; i < toppingArr.length; i++) {
    if (this.topping[topping.name].name === toppingArr[i]) {
      console.log("Неправильні данні топінгу");
      delete this.topping[topping.name];
    }
  }
};

Hamburger.prototype.removeTopping = function (topping) {
  this.topping_name = topping.name;

  if (this.topping[topping.name] === undefined) {
    console.log("Топінг ще не було додано або невірні данні");
  } else {
    delete this.topping[topping.name];
  }
};
