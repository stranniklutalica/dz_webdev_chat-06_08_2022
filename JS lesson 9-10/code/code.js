//ДЗ: Згенерувати коментарі з кнопкою редагування. При натисканні на яку відкриваеться модальне вікно з коментарем

/* Розмітка взяв з Роботи 5-6  і на її основі генерував нову 
comments.forEach(user => {
    document.write(`
    <div class="flex">
    <div class="name">
        <p># ${user.id}</p>
        <p>Autor: ${user.name}</p>
        <a href="mailto:">${user.email}</a>
    </div>
    <div class="body">
        <p>${user.body}</p>
    </div>
</div>
`)
*/


    function createElement (tagName, tegClass, text, attribute) {
        const el = document.createElement(tagName);
        el.textContent = text;
        if (tegClass){
            el.classList.add(tegClass);
        }
        if (attribute){
            el.setAttribute("href", attribute);
        }
        
        return el;
    };



for(let key in comments){
    const {id, name, email, body} = comments[key];
// створюю основні Div
    const fl = createElement("div", "f" + key);
    fl.classList.add("flex");
    const nm = createElement("div", "n" + key);
    nm.classList.add("name");
    const b = createElement("div", "b" + key);
    b.classList.add("body");
    const p = createElement("p", "", body);
    p.id = "id" + key;
     const btn = createElement("button");
    btn.textContent = "Редагувати";
    btn.onclick = function (){
        document.querySelector(".modal").style.display = "block";
        document.querySelector(".modal").innerHTML = body;
    
    }

   
// вивожу основні діви в ДОМ-дерево
    document.querySelector(".root").append(fl);        
    document.querySelector(".f" + key).append(nm, b)
//створюю вкладені Div
    const arrUser = [createElement("p", "", "#" + id), createElement("p", "", "Author: " + name), createElement("a", "", email, "mailto:")];

// вивожу вкладені діви в ДОМ-дерево
    document.querySelector(".n" + key).append(...arrUser)
    document.querySelector(".b" + key).append(p, btn)

   
}

