import {
    createCategoryInputModal,
    generatorID,
    editInputProduct,
    closeWindowModal
  } from "./functions.js";
//import { saveMyBtnClick } from "./myFunction.js";

  if (!localStorage.BDVideo) {
    localStorage.BDVideo = JSON.stringify([]);
  } 
  
const section = document.querySelector("section")
const modal =  `<div class="container-modal">
<div class="modal">
    <form class="input-edit">

    </form>
    <div class="form-btn">
        <button type="button" id="save">Зберегти</button>
        <button type="button" id="close">Закрити</button>
    </div>
</div>
</div>`
section.insertAdjacentHTML("beforeEnd", modal)

  let categoryName = null;
  const dataVideo = JSON.parse(localStorage.BDVideo)
const btnModal = document.getElementById("btn-modal"),
selectModal = document.getElementById("select-data"),
btnClose = document.getElementById("close"),
btnSave = document.getElementById("save"),

modalWindow = document.querySelector(".container-modal"),

categoryInfo = document.querySelector(".category-info");

if(document.location.pathname.search("video") == -1 && document.location.pathname.search("restoran") == -1 ){

selectModal.addEventListener("change", (e) => {
  categoryInfo.innerHTML = ""
  if (e.target.value === "Ресторан") {
    categoryName = "Ресторан";
    categoryInfo.insertAdjacentHTML("afterbegin", createCategoryInputModal(restoration).join(""));
  } else if (e.target.value === "Магазин") {
    categoryName = "Магазин";
    categoryInfo.insertAdjacentHTML("afterbegin",  createCategoryInputModal(store).join("")
    );
  } else if (e.target.value === "Відео хостинг") {
    categoryName = "Відео хостинг";
    categoryInfo.insertAdjacentHTML("afterbegin",  createCategoryInputModal(video).join("")
    )};
});

}





btnSave.addEventListener("click", (e) => {
 console.log("=====");
 
   

  if(categoryName === "Відео хостинг"){
    let objInfo = {
      id: "",
      productName: "",
      porductDate: "",
      productLink: "",
      productDescription: "",
      keywords: [],
    };
    validateInput(objInfo);
  }    
  
});


const restoration = [
  "Назва продукта",
  "Введіть грамовку",
  "Введіть склад",
  "Вартість страви",
  "Зобреження",
  "Гарячі слова, розділяйте комою.",
  "Вага фінальна",
];
const video = [
  "Назва фільму",
  "Дата публікації",
  "Посилання",
  "Опис фільму",
  "Гарячі слова, розділяйте комою."
  
];
const store = [
  "Назва продукта",
  "Введіть вартість",
  "Посилання на зображення",
  "Опис товару",
  "Гарячі слова, розділяйте комою."
];


function validateInput(obj) {
  const [...inputs] = document.querySelectorAll(".category-info input");
  inputs.forEach((el) => {
    if (el.value.length >= 3) {
      obj.id = generatorID();
      obj.dataAdd = `${new Date().getFullYear()}-${
        new Date().getMonth() + 1
      }-${new Date().getDate()} ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`;
      if (el.dataset.type === "Назва фільму") {
        obj.productName = el.value;
      } else if (el.dataset.type === "Дата публікації") {
        obj.porductDate = el.value;
      } else if (el.dataset.type === "Посилання") {
        obj.productLink = el.value;
      } else if (el.dataset.type === "Опис фільму") {
        obj.productDescription = el.value;
      } else if (el.dataset.type === "Гарячі слова, розділяйте комою.") {
        obj.keywords.push(...el.value.split(","));
      }
      el.value = "";
      el.classList.remove("error");
    } else {
      el.classList.add("error");
      return;
    }
  });

  let data = JSON.parse(localStorage.BDVideo);
  data.push(obj);
  localStorage.BDVideo = JSON.stringify(data);
}




function showMyProduct() {
  
  if (document.location.pathname.search("video") !== -1) {
      const tbody = document.querySelector("table tbody");
      tbody.innerHTML = ""
      tbody.insertAdjacentHTML("beforeend", dataVideo.map((infoProduct, index) => {
          return `
          <tr>
              <td>${index + 1}</td>
              <td>${infoProduct.productName}</td>
              <td title="При настиску сортувати.">${infoProduct.porductDate}</td>
              <td title="При настиску сортувати."><a href="${infoProduct.productLink}" target="_blank">${infoProduct.productLink}</td>
              <td class="edit" data-id="${index}">&#128221;</td>
              <td>${infoProduct.status ? "&#9989;" : "&#10060;"}</td>
              <td>${infoProduct.dataAdd ? infoProduct.dataAdd : "Без дати"}</td>
              <td>&#128465;</td>
          </tr>`
      }).join(""))
  }
  const [...edits] = document.querySelectorAll(".edit");
  edits.forEach((tdEdit) => {
      tdEdit.addEventListener("click", (e) => {
          modalWindow.classList.add("active");
          editProduct(dataVideo[tdEdit.dataset.id], document.location.pathname)
      })
  })
}

showMyProduct()

const editProduct = (product, url = "") => {

  if (url.includes("video")) {
    
      const { id, keywords, productName, porductDate, productDescription, productLink } = product;
      const inputs = [
          editInputProduct("text", keywords, "Ключеві слова", generatorID(), ""),
          editInputProduct("number", porductDate, "Дата публікації", generatorID(), ""),
          editInputProduct("text", productName, "Назва фільму", generatorID(), ""),
          editInputProduct("text", productLink, "Посилання", generatorID(), ""),
          editInputProduct("text", productDescription, "Гарний опис продукції", generatorID(), "")
          
         
      ]
      document.querySelector(".input-edit").dataset.key = id;
      document.querySelector(".input-edit").innerHTML = "";
      document.querySelector(".input-edit").append(...inputs)
  }

}

btnSave.addEventListener("click", saveMyBtnClick)
btnClose.addEventListener("click", closeWindowModal)


function saveMyBtnClick () {
 // btnSave.removeEventListener("click", saveBtnClick)

  const keyForm = document.querySelector(".input-edit").dataset.key;
  const inputs = document.querySelectorAll(".input-edit input");
  const obj = {};
  
  if(document.location.pathname.search("video") !== -1){
    let data =JSON.parse(localStorage.BDVideo)
      inputs.forEach((input) => {
          if (input.key === "Ключеві слова") {
              obj.keywords = input.value.split(",")
          } else if (input.key === "Дата публікації") {
              obj.porductDate = input.value;
          } else if (input.key === "Назва фільму") {
              obj.productName = input.value;
          
          } else if (input.key === "Гарний опис продукції") {
              obj.productDescription = input.value;
          } else if (input.key === "Посилання") {
              obj.productLink = input.value;
          } 
      })
      const [rez] = data.filter((el, i) => {
          return el.id === keyForm
      })
      obj.id = rez.id;
      obj.data = rez.data;
  
      data.forEach((el, i) => {
          if (el.id === rez.id) {
            dataVideo.splice(i, 1, obj)
          }
      })
      localStorage.BDVideo = JSON.stringify(data);  
    }
  
  
  showMyProduct()
  closeWindowModal()
}
