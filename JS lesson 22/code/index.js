//aaa      +++
//#sss     +++
//.ddd      +++
//aaa#sss   +++
//aaa.sss     +++
//aaa#sss.ddd   +++
//#sss.ddd      +++
//aaa.sss#ddd   +++
//.sss#ddd      +++
//aaa#sss{hhh}  +++
//aaa.sss{hhh}  +++
//aaa.sss#ddd{hhh}  +++
//aaa#sss.fff{hhh} +++
//#sss.fff{hhh}   +++
//.sss#fff{hhh}   +++

const textarea = document.querySelector(".selector");
const teg = document.querySelector(".teg");

textarea.addEventListener("input", (e) => {
  let text = e.target.value;

  const regClass = /[\.]{1}/;
  const regID = /#{1}/;
  const regText = /{/;
  let newTeg = null;
  const regFull = /\.|#|{/g;

  if (regFull.test(text)) {
    //перевірка на класс
    if (regClass.test(text)) {
      let arr = text.split(regClass);
      if (arr[0] === "") {
        newTeg = createTeg("div");
        newTeg.classList.add(arr[1]);
      } else {
        if (regID.test(arr[0])) {
          newTeg = testID(arr[0]);
          newTeg.classList.add(arr[1]);
        } else {
          newTeg = createTeg(arr[0]);
          newTeg.classList.add(arr[1]);
        }
      }
    }
    //перевірка на ІД
    if (regID.test(text)) {
      let arr = text.split(regID);
      if (arr[0] === "") {
        if (!regClass.test(arr[1])) {
          newTeg = createTeg("div");
          newTeg.id = arr[1];
        }
      } else {
        if (regClass.test(arr[0])) {
          newTeg = testClass(arr[0]);
          newTeg.id = arr[1];
        } else {
          if (!regClass.test(arr[1])) {
            newTeg = createTeg(arr[0]);
            newTeg.id = arr[1];
          }
        }
      }
    }
    //перевірка на текст
    if (regText.test(text)) {
      let arr = text.split(regText);
      if (arr[0] !== "") {
        if (regClass.test(arr[0])) {
          newTeg = testClass(arr[0]);
          newTeg.innerText = arr[1];
        } else {
          if (regID.test(arr[0])) {
            newTeg = testID(arr[0]);
            newTeg.innerText = arr[1];
          }
        }
      }
    }
  } else {
    //вивід тега без нічого
    newTeg = createTeg("div");
    newTeg.innerText = text;
  }

  showTeg(newTeg);
});

function testClass(newtext) {
  let arr = newtext.split(/[\.]{1}/);
  if (arr[0] === "") {
    newTeg = createTeg("div");
    if (!/#{1}/.test(arr[1])) {
      newTeg.classList.add(arr[1]);
    } else {
      let arrid = arr[1].split(/#/);
      newTeg.classList.add(arrid[0]);
      newTeg.id = arrid[1];
    }
  } else {
    if (!/#{1}/.test(arr[0])) {
      newTeg = createTeg(arr[0]);
      if (/#/.test(arr[1])) {
        let arrid = arr[1].split(/#/);
        if (arrid[0] === "") {
          newTeg = createTeg("div");
          newTeg.classList.add(arr[0]);
          newTeg.id = arrid[1];
        } else {
          newTeg.id = arrid[1];
          newTeg.removeAttribute("class");
          newTeg.classList.add(arrid[0]);
          console.log('teg', newTeg);
        }
      };
    } else {
      if (/#/.test(arr[0])) {
        let arrid = arr[0].split(/#/);
        if (arrid[0] === "") {
          newTeg = createTeg("div");
          newTeg.classList.add(arr[1]);
          newTeg.id = arrid[1];
        } else {
          newTeg.id = arrid[1];
          newTeg.removeAttribute("class");
          newTeg.classList.add(arr[1]);
        }
      } else {
        let arrid = arr[1].split(/#/);
        newTeg.id = arrid[1];
      }
    }
  }
  return newTeg;
}

function testID(newtext) {
  let arr = newtext.split(/#/);
  if (arr[0] === "") {
    newTeg = createTeg("div");
    newTeg.id = arr[1];
  } else {
    newTeg = createTeg(arr[0]);
    newTeg.id = arr[1];
  }
  return newTeg;
}

function createTeg(tegName) {
  let teg = document.createElement(tegName);
  return teg;
}

function showTeg(obj) {
  teg.innerText = obj.outerHTML;
}
