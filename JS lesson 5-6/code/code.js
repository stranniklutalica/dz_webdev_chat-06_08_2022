//  ДЗ : 5
/* Створити об'єкт "Документ", де визначити властивості "Заголовок, тіло, футер, дата". 
Створити об'єкт вкладений об'єкт - "Додаток". 
Створити об'єкт "Додаток", вкладені об'єкти, "Заголовок, тіло, футер, дата". 
Створити методи для заповнення та відображення документа.
*/

const doc = {
    header: "заголовок",
    main: "тіло",
    footer: "футер",
    data: "дата",
    addition:{
        header:{header: "заголовок2"} ,
        main:{main: "тіло2"},
        footer:{footer: "футер2"},
        data:{data: "дата2"}
    }
    
};
function funct() {
    for (let key in doc) {        
        if (typeof (doc[key]) == `object`) {
            for (let key1 in doc){                
                if (typeof (doc[key][key1]) == `object`){
                    for (let key2 in doc[key][key1]){
                        document.write(key  + ` -> ` + key1  + ` -> ` + key2 + ` : ` + doc[key][key1][key2] + "<br>");                    
                } 
                } else {
                    document.write(key  + ` -> ` + key1 + ` : ` + doc[key][key1] + "<br>");
                };                
            }            
        }  else {
            document.write(key + ` : ` + doc[key] + "<br>");
    };
    }
}

funct();



// Створіть сторінку коментарів та стилізуйте її як сторінка відгуків. 
// За допомогою методів перебору виведіть на сторінку коментарі, пошту, номер коментаря та автора


comments.forEach(user => {
    
    document.write(`
    <div class="flex">
    <div class="name">
        <p># ${user.id}</p>
        <p>Autor: ${user.name}</p>
        <a href="mailto:">${user.email}</a>
    </div>
    <div class="body">
        <p>${user.body}</p>
    </div>
</div>
`)

})


//  Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.
const arr = [10,20,30,60,100];
let newArr = [];
function fn(a){
    let cr = a + 10;
    return cr
 };
function map(fn, arr){    
    for(let i = 0; i < arr.length; i++){
    newArr.push(fn(arr[i]))}
    return newArr};
map(fn, arr)
 console.log(`Старий массив: ${arr}`);
 console.log(`функція додає 10`)
 console.log(`Новий массив: ${newArr}`);

