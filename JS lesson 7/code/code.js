// ДЗ🎓

// Створіть клас Phone, який містить змінні number, model і weight.
// Створіть три екземпляри цього класу.
// Виведіть на консоль значення їх змінних.
// Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.



class Phone{
    constructor(number, model, weight){
        this.number = number;
        this.model = model;
        this.weight = weight;
    }
    receiveCall(name) {        
       return console.log(`Телефонує ${name}`)
    }
    getNumber (tel) {
        return console.log(`tel:${tel}`)
        
    }    
};

let moto = new Phone(10, "moto", 150)
let nokia = new Phone(222, "nokia", 250)
let sony = new Phone(35, "sony", 170)
let call = new Phone()
let phoneNumber = new Phone()

console.log(`Model:${moto.model}  Number:${moto.number} Вес:${moto.weight}`)
console.log(`Model:${nokia.model}  Number:${nokia.number} Вес:${nokia.weight}`)
console.log(`Model:${sony.model}  Number:${sony.number} Вес:${sony.weight}`)
call.receiveCall(`Alex`)
phoneNumber.getNumber(`12346567`)


// - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].


let arr = [1, 2 ,`hello`, true]
function filterBy(array, type){
    console.log(`Відаляємо тип ${type}`)
    let ar = arr.filter(function(a) {
        return typeof(a) !== type
      });
    return ar
} 
console.log(arr)
let newAar = filterBy(arr, `number`)
console.log(newAar)




