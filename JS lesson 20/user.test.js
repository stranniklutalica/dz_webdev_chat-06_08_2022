const user = require("./User");
const ivan = new user("100", "ivan", "ivan@M.cc", "pas");
test("Test getId", () => {
  expect(ivan.getId()).toBe("100");
});
test("Test getName", () => {
  expect(ivan.getName()).toBe("ivan");
});
test("Test getEmail", () => {
  expect(ivan.getEmail()).toBe("ivan@M.cc");
});
test("Test setEmail", () => {
  ivan.setEmail("test@m.cc")
  expect(ivan.getEmail()).toBe("test@m.cc");
});
test("Test setPassword", () => {
  ivan.setPassword('testpas')
  expect(ivan.password).toBe("testpas");
});