class User {
  constructor(id, names, email, password) {
    this.id = id;
    this.names = names;
    this.email = email;
    this.password = password;
  }
  getId() {
    return this.id;
  }
  getName() {
    return this.names;
  }
  getEmail() {
    return this.email;
  }
  setEmail(email) {
    this.email = email;
  }
  setPassword(password) {
    this.password = password;
  }
}

module.exports = User;

// Ваша задача - реалізувати клас User на JavaScript, який буде представляти користувача вашого веб-додатка.

// Клас має мати наступні поля:

// id - унікальний ідентифікатор користувача;
// name - ім'я користувача;
// email - email користувача;
// password - пароль користувача.

// Клас повинен мати наступні методи:
// getId() - повертає id користувача;
// getName() - повертає ім'я користувача;
// getEmail() - повертає email користувача;
// setEmail(email) - встановлює email користувача;
// setPassword(password) - встановлює пароль користувача.

// Ви повинні написати юніт-тест (unit test) для класу User, який перевіряє наступні речі:

// Конструктор класу User повинен створювати об'єкт з правильними властивостями;
// Методи getId(), getName(), getEmail() повинні повертати відповідні властивості об'єкту;
// Методи setEmail(email) та setPassword(password) повинні правильно встановлювати властивості об'єкту.

