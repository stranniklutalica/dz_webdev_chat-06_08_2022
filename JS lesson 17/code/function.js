const spans = [];
const display = document.querySelector(".display");
async function req(url) {
  document.querySelector(".loader").classList.add("active");
  const data = await fetch(url);
  return data.json();
}

function show(data) {
  display.innerHTML = "";
  const table = `
        <table>
         <thead>
          <tr>
           <th>
           №
           </th>
           <th>
           Задача
           </th>
           <th>
           Статус
           </th>
           <th>
           Редагувати
           </th>
          </tr>
         </thead>
         <tbody>
           ${data
             .map((obj, i) => {
               const span = document.createElement("span");
               span.addEventListener("click", () => {
                 clickHendler(obj);
               });
               span.innerHTML = "&#128295;";
               spans.push(span);
               return `
               <tr>
                    <td>
                    ${obj.id}
                    </td>
                    <td>
                    ${obj.title}
                    </td>
                    <td>
                    ${obj.completed ? "&#9989;" : "&#10060;"}
                    </td>
                    <td data-span="${i}">
                      
                    </td>
                </tr>`;
             })
             .join("")}
         </tbody>
         </tablr>
        `;
  display.insertAdjacentHTML("beforeend", table);

  document.querySelectorAll("td[data-span]").forEach((e, i) => {
    e.append(spans[i]);
  });
  document.querySelector(".loader").classList.remove("active");
}

function clickHendler(obj) {
  document.querySelector(".parent").classList.add("active");
  document.getElementById("description").innerText = obj.title;
  document.getElementById("status").checked = obj.completed;

  document.getElementById("save").onclick = () => {
    console.log("+");
    const l = JSON.parse(localStorage.history);
    l.push(obj);
    localStorage.history = JSON.stringify(l);

    document
      .querySelector("#save")
      .addEventListener("click", (e) =>
        document.querySelector(".parent").classList.remove("active")
      );
  };
}

function swShow(info) {
  const data = info.results;

  display.innerHTML = "";
  const table = `
        <table>
         <thead>
          <tr>
           <th>
           Ім'я
           </th>
           <th>
           Зріст
           </th>
           <th>
           Вага
           </th>
           <th>
           Колір волосся
           </th>
          </tr>
         </thead>
         <tbody>
         ${data
           .map((obj, i) => {
             return `
                   <tr>
                        <td>
                        ${obj.name}
                        </td>
                        <td>
                        ${obj.height}
                        </td>
                        <td>
                        ${obj.mass}
                        </td>
                        <td>
                        ${obj.hair_color}
                        </td>
                       
                    </tr>`;
           })
           .join("")}
         </tbody>
         </tablr>
        `;
  display.insertAdjacentHTML("beforeend", table);

  document.querySelector(".loader").classList.remove("active");
}

function nbuShow(data) {
  display.innerHTML = "";

  const table = `
        <table>
         <thead>
         <thead>Валюти на ${data[0].exchangedate}</thead>
          <tr>
           <th>
           </th>
           <th>
           Валюта
           </th>
           <th>
           Курс
           </th>
         
          </tr>
         </thead>
         <tbody>
         ${data
           .map((obj, i) => {
             return `
                   <tr>
                        <td>
                        ${obj.cc}
                        </td>
                        <td>
                        ${obj.txt}
                        </td>
                        <td>
                        ${obj.rate.toFixed(2)}
                        </td>
                     
                       
                    </tr>`;
           })
           .join("")}
         </tbody>
         </tablr>
        `;
  display.insertAdjacentHTML("beforeend", table);

  document.querySelector(".loader").classList.remove("active");
}

export { req, show, swShow, nbuShow };
