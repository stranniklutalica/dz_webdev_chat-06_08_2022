import { req, show, swShow, nbuShow } from "./function.js";
/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 
Курс валют НБУ з датою на який день,  https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json
героїв зоряних війн, https://swapi.dev/api/people/
список справ з https://jsonplaceholder.typicode.com/ виводити які виконані які та які ні з можливістю редагування
*/
if (localStorage.history === undefined) {
  localStorage.history = JSON.stringify([]);
}

document.getElementById("todo").addEventListener("click", () => {
  req("https://jsonplaceholder.typicode.com/todos").then((info) => show(info));
});

document.getElementById("sw").addEventListener("click", () => {
  req("https://swapi.dev/api/people/").then((info) => swShow(info));
});

document.getElementById("nbu").addEventListener("click", () => {
  req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json").then(
    (info) => nbuShow(info)
  );
});

document
  .querySelector("#close")
  .addEventListener("click", (e) =>
    document.querySelector(".parent").classList.remove("active")
  );
