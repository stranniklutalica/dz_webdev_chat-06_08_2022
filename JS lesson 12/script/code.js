window.addEventListener("DOMContentLoaded", () => {
  let x = ``,
    y = ``,
    znak = ``,
    fin = false,
    mem = ``;

  document.getElementById("keys").addEventListener(
    "click",
    (e) => {
      const keyDown = e.target.getAttribute("value");
      const displey = document.getElementById("dsp");

      const key = keyDown.search(/[0-9.]/g);
      const z = keyDown.search(/[+\-*/]/gy);

      //обробка дорівнює та матиматика
      if (keyDown === `=`) {
        switch (znak) {
          case `+`: {
            x = +x + +y;
            displey.value = x;

            break;
          }
          case `-`: {
            x = +x - +y;
            displey.value = x;

            break;
          }
          case `*`: {
            x = +x * +y;
            displey.value = x;

            break;
          }
          case `/`: {
            if (y == `0`) {
              displey.value = `Ділення на 0`;
              (x = ``), (y = ``), (znak = ``), (fin = false);
              break;
            } else {
              x = +x / +y;
              displey.value = x;

              break;
            }
          }
        }
        fin = true;
        y = ``;
        znak = ``;
      }
      //заповнення x,y
      if (key >= 0) {
        if (x === `` || (y === `` && znak === ``)) {
          x += keyDown;
          displey.value = x;
        } else if (x !== `` && y !== `` && fin) {
          x = keyDown;
          displey.value = x;
        } else {
          y += keyDown;
          displey.value = y;
        }
      }
      //знаходження знаку
      if (z >= 0) {
        znak = keyDown;
        displey.value = keyDown;
      }
      //обробка сбросу
      if (keyDown == `C`) {
        x = ``;
        y = ``;
        znak = ``;
        fin = false;
        displey.value = ``;
      }
      //обробка памьяті
      if (keyDown === `m+`) {
        mem = +mem + +displey.value;
        x = ``;
      }
      if (keyDown === `m-`) {
        mem = +mem - +displey.value;
        x = ``;
      }
      if (keyDown === `mrc`) {
        if (x === ``) {
          x = mem;
          displey.value = x;
          mem = ``;
        } else {
          y = mem;
          displey.value = y;
          mem = ``;
        }
      }
    },
    false
  );
});
