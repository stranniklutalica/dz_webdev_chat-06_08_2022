import cake from "./cake.js";

const prod = document.querySelector(".prod");
const bagCost = document.querySelector(".bag_cost");
const basketNum = document.querySelector(".basketNum");
const check = document.getElementById("checkout");
const btnshop = document.querySelector(".main");
const bag = document.querySelector(".bag");
const basket = document.getElementById("basket");

let productBag;
if (localStorage.getItem("basket")) {
  productBag = JSON.parse(localStorage.getItem("basket"));
} else {
  localStorage.setItem("basket", JSON.stringify({}));
  productBag = JSON.parse(localStorage.getItem("basket"));
}
// корзина
basket.addEventListener("click", () => {
  let fullCost = 0;
  bag.classList.toggle("hide");
  prod.innerHTML = "";
  for (let key in productBag) {
    fullCost += Number(cake[key].cost * productBag[key]);
    prod.insertAdjacentHTML(
      "afterbegin",
      `<div class="bag_intentory">
          <div class="bag_intentory_list">
            <div class="flex">
              <div>
                <img  class="bag_intentory_list-img" src="../image/cake/cake${key}.png" alt="cake">
              </div>
              <div class="bag_inventory_list-name">
              ${cake[key].name} x
              </div>
              <div class="bag_inventory_list-quantity">${productBag[key]}</div>
              
            </div>
            <div class="bag_inventory_list-cost">$${(
              cake[key].cost * productBag[key]
            ).toFixed(2)}</div>
            </div>
          </div>
          <div class="bag_line"></div>
          
  `
    );
    bagCost.innerHTML = "$" + fullCost.toFixed(2);
  }
});
basketNumber();


check.addEventListener("click", (e) => {
  window.open("../basket", "_self");
});

btnshop.addEventListener("click", (e) => {
  if (e.target.classList == "Text_button") {
    window.open("../index.html#grid", "_self");
  }
});

function basketNumber() {
  basketNum.textContent = Object.keys(
    JSON.parse(localStorage.getItem("basket"))
  ).length;
}
