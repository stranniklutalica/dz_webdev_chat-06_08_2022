import cake from "./cake.js";

let wow = new WOW({
  boxClass: "wow", // animated element css class (default is wow)
  animateClass: "animated", // animation css class (default is animated)
  offset: 0, // distance to the element when triggering the animation (default is 0)
  mobile: true, // trigger animations on mobile devices (default is true)
  live: true, // act on asynchronously loaded content (default is true)
  callback: function (box) {
    // the callback is fired every time an animation is started
    // the argument that is passed in is the DOM node being animated
  },
  scrollContainer: null, // optional scroll container selector, otherwise use window,
  resetAnimation: true, // reset animation on end (default is true)
});
wow.init();
//------------ grid cards
const grid = document.getElementById("grid");
const prod = document.querySelector(".prod");
const bagCost = document.querySelector(".bag_cost");
const basketNum = document.querySelector(".basketNum");
const bag = document.querySelector(".bag");
const basket = document.getElementById("basket");

let productBag;
if (localStorage.getItem("basket")) {
  productBag = JSON.parse(localStorage.getItem("basket"));
} else {
  localStorage.setItem("basket", JSON.stringify({}));
  productBag = JSON.parse(localStorage.getItem("basket"));
}

grid.addEventListener("click", (e) => {
  let target = e.target;
  let dataKey = target.getAttribute("data-value");

  let quantity;

  if (!dataKey) {
    return;
  }

  // логіка доавання продуктів
  document.querySelectorAll("span.quantity[data-value]").forEach((element) => {
    if (element.dataset.value == dataKey) {
      quantity = element;
    }
  });

  if (quantity.getAttribute("data-value") === dataKey) {
    if (e.target.classList.value == "plus") {
      ++quantity.textContent;
      productBag[dataKey] = quantity.textContent;
    }
    if (e.target.classList.value == "minus") {
      if (quantity.textContent !== "0") {
        --quantity.textContent;
        if (quantity.textContent === "0") {
          delete productBag[dataKey];

          basketNumber();
        } else {
          productBag[dataKey] = quantity.textContent;
        }
      }
    }
  }

  if (e.target.getAttribute("data-type") == "btn") {
    console.log(e.target);
   
    localStorage.setItem("basket", JSON.stringify(productBag));
    basketNumber();
    

  }
});

// корзина
basket.addEventListener("click", () => {
  let fullCost = 0;
  bag.classList.toggle("hide");
  prod.innerHTML = "";
  for (let key in productBag) {
    fullCost += Number(cake[key].cost * productBag[key]);
    prod.insertAdjacentHTML(
      "afterbegin",
      `<div class="bag_intentory">
          <div class="bag_intentory_list">
            <div class="flex">
              <div>
                <img  class="bag_intentory_list-img" src="./image/cake/cake${key}.png" alt="cake">
              </div>
              <div class="bag_inventory_list-name">
              ${cake[key].name} x
              </div>
              <div class="bag_inventory_list-quantity">${productBag[key]}</div>
              
            </div>
            <div class="bag_inventory_list-cost">$${(
              cake[key].cost * productBag[key]
            ).toFixed(2)}</div>
            </div>
          </div>
          <div class="bag_line"></div>
          
  `
    );
    bagCost.innerHTML = "$" + fullCost.toFixed(2);
  }
});


basketNumber();


//меню продуктів
for (let key in cake) {
  grid.insertAdjacentHTML(
    "afterbegin",
    `
  <div class="card" id="${key}">
  <div class="card_img">
    <img src="${cake[key].img}" alt="cake">
  </div>
  <div class="card_title">${cake[key].name}</div>
  <div class="card_text">${cake[key].text}</div>
  <div class="card_shop">
    <div class="card_add">
    <span class="minus" data-value="${key}">-</span>
    <span class="quantity" data-value="${key}">${
      !productBag[key] ? "0" : productBag[key]
    }</span>
    <span class="plus" data-value="${key}">+</span>
    
  </div>
  <button  type="button" class="add" data-value="${key}" data-type="btn">add to cart</button>
  </div>
  </div>
  `
  );
}

function basketNumber() { //кількість продуктів
  basketNum.textContent = Object.keys(
    JSON.parse(localStorage.getItem("basket"))
  ).length;
}

const check = document.getElementById("checkout");// кнопка купити
check.addEventListener("click", (e) => {
  window.open("./basket", "_self");
});
