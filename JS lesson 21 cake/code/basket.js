import cake from "./cake.js";

const prod = document.querySelector(".prod");
const bagCost = document.querySelector(".bag_cost");
const basketNum = document.querySelector(".basketNum");
const product = document.querySelector(".product");
const productCost = document.querySelector(".prod_cost");
const bag = document.querySelector(".bag");
const basket = document.querySelector(".basket");
const main = document.querySelector("#main");
const order = document.getElementById("btn_order");
let productBag;
let fullCost = 0;

if (localStorage.getItem("basket")) {
  productBag = JSON.parse(localStorage.getItem("basket"));
} else {
  localStorage.setItem("basket", JSON.stringify({}));
  productBag = JSON.parse(localStorage.getItem("basket"));
}
// відрисовка корзіни
function view_product() {
  fullCost = 0;
  product.innerHTML = "";
  for (let key in productBag) {
    fullCost += Number(cake[key].cost * productBag[key]);
    product.insertAdjacentHTML(
      "afterBegin",
      `
  <div class="basket_list">
            <div class="basket_list_img"><img src="../image/cake/cake${key}.png" alt="cake"></div>
            <div class="basket_text">
              <div class="basket_list_name">${cake[key].name}</div>
              <div class="card_shop">
              <div class="card_add">
              <span class="minus" data-value="${key}">-</span>
              <span class="quantity" data-value="${key}">${
        !productBag[key] ? "0" : productBag[key]
      }</span>
              <span class="plus" data-value="${key}">+</span>
              
            </div>
            <div class="basket_list_cost">$${(
              cake[key].cost * productBag[key]
            ).toFixed(2)}</div>
            </div>
            
          </div>
  `
    );
  }

  productCost.innerHTML = "$" + fullCost.toFixed(2);
}

view_product();
// логіка корзини
product.addEventListener("click", (e) => {
  let target = e.target;
  let dataKey = target.getAttribute("data-value");
  let quantity;
  if (!dataKey) {
    return;
  }
  document.querySelectorAll("span.quantity[data-value]").forEach((element) => {
    if (element.dataset.value == dataKey) {
      quantity = element;
    }
  });

  if (quantity.getAttribute("data-value") === dataKey) {
    if (e.target.classList.value == "plus") {
      ++quantity.textContent;
      productBag[dataKey] = quantity.textContent;
      localStorage.setItem("basket", JSON.stringify(productBag));
      view_product();
    }
    if (e.target.classList.value == "minus") {
      if (quantity.textContent !== "0") {
        --quantity.textContent;

        if (quantity.textContent === "0") {
          delete productBag[dataKey];
          localStorage.setItem("basket", JSON.stringify(productBag));

          basketNumber();
        } else {
          productBag[dataKey] = quantity.textContent;
          localStorage.setItem("basket", JSON.stringify(productBag));
        }
      }
      view_product();
    }
  }
});
// корзина
basket.addEventListener("click", () => {
  bag.classList.toggle("hide");
  prod.innerHTML = "";
  for (let key in productBag) {
    prod.insertAdjacentHTML(
      "afterbegin",
      `<div class="bag_intentory">
          <div class="bag_intentory_list">
            <div class="flex">
              <div>
                <img  class="bag_intentory_list-img" src="../image/cake/cake${key}.png" alt="cake">
              </div>
              <div class="bag_inventory_list-name">
              ${cake[key].name} x
              </div>
              <div class="bag_inventory_list-quantity">${productBag[key]}</div>
              
            </div>
            <div class="bag_inventory_list-cost">$${(
              cake[key].cost * productBag[key]
            ).toFixed(2)}</div>
            </div>
          </div>
          <div class="bag_line"></div>
          
  `
    );
  }
  bagCost.innerHTML = "$" + fullCost.toFixed(2);
});


basketNumber();
productCost.innerHTML = "$" + fullCost.toFixed(2);

function basketNumber() {
  basketNum.textContent = Object.keys(
    JSON.parse(localStorage.getItem("basket"))
  ).length;
}

order.addEventListener("click", (e) => {
  alert("Еhe order has been sent");
  productBag = {};
  localStorage.setItem("basket", JSON.stringify({}));
  view_product();
  basketNumber();

  main.innerHTML = "Thank you!";
  main.classList.add("thank");
});
