const cake = {
  "1": {
    name: "Carrot",
    img: "./image/cake/cake11.png",
    text: "Walnut-studded carrot cake with cinnamon cream cheese frosting",
    cost: "3.20",
  },
  "2": {
    name: "Red Velvet",
    img: "./image/cake/cake1.png",
    text: "A chocolate sponge, coloured naturally with beetroot and topped off with cream cheese",
    cost: "3.20",
  },
  "3": {
    name: "Mint Chip",
    img: "./image/cake/cake2.png",
    text: "Homemade chocolate cupcakes topped with thick & creamy mint chocolate chip frosting.",
    cost: "3.20",
  },
  "4": {
    name: "Pink strawberry",
    img: "./image/cake/cake4.png",
    text: "A fluffy strawberry cupcake with strawberry buttercream frosting and chocolate syrup. ",
    cost: "3.20",
  },
  "5": {
    name: "Marshmallow",
    img: "./image/cake/cake5.png",
    text: "A super chocolatey cupcake base with a soft marshmallowy buttercream topping",
    cost: "3.20",
  },
  "6": {
    name: "Dark Chocolate",
    img: "./image/cake/cake6.png",
    text: "Belgian chocolate cake with sweet chocolate frosting and cheery.",
    cost: "3.20",
  },
  "7": {
    name: "Salty Caramel",
    img: "./image/cake/cake7.png",
    text: "Caramel cake with a buttery caramel cream cheese frosting topped with fleur de sel.",
    cost: "3.20",
  },
  "8": {
    name: "Gluten Free Velvet",
    img: "./image/cake/cake8.png",
    text: "A gluten free twist on our classic red velvet, topped off with cream cheese.",
    cost: "3.20",
  },
  "9": {
    name: "Cinnamon",
    img: "./image/cake/cake9.png",
    text: "Lightly spiced buttermilk cake with cinnamon cream cheese frosting with cinnamon sugar.",
    cost: "3.20",
  },
  "10": {
    name: "Totally nuts",
    img: "./image/cake/cake10.png",
    text: "A sweet hazelnut paste with nutella butter cream and chopped hazelnuts on the top.",
    cost: "3.20",
  },
  "11": {
    name: "Pink Vanilla",
    img: "./image/cake/cake3.png",
    text: "Soft, fluffy, and extra moist creamy vanilla buttercream with extra sprinkles ",
    cost: "3.20",
  }
 
};


export default cake