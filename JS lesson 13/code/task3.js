// 3.

// Ім'я, Прізвище (Українською)
// Список з містами України
// Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора.
// Пошта
// Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅

// Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані

window.addEventListener("DOMContentLoaded", () => {
  const sel = document.getElementById("city");

  const kievstar = "39,67,68,96,97,98",
    vodafone = "50,66,95,99",
    life = "63,73,93";

  for (let key in city) {
    let citys = city[key].city;
    let option = document.createElement("option");
    option.textContent = citys;
    sel.append(option);
  }

  const span = document.createElement("span");
  let regPart;
   const [...inputs] = document.querySelectorAll("input");

  document.getElementById("sub").addEventListener("click", validSubmit);

  inputs.forEach((element) => {
    document.getElementById(element.id).addEventListener("change", function () {
      if (element.id == `names`) {
        regPart = /^[А-я-іІїЇ]+ [А-я-іІїЇ]+$/;
      } else if (element.id == `tel`) {
        element.classList = "operator";
        regPart = /^\+380\d\d \d\d\d \d\d \d\d$/;
        let logoOp = element.value.substring(4, 6);
        if (kievstar.search(logoOp) >= 0) {
          element.classList.add("kievstar");
        } else if (vodafone.search(logoOp) >= 0) {
          element.classList.add("vodafone");
        } else if (life.search(logoOp) >= 0) {
          element.classList.add("life");
        }
      } else {
        regPart = /[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}/i;
      }

      valid(regPart, this.value);
      this.after(span);
    });
  });

  function valid(reg, value) {
    if (reg.test(value) == false) {
      span.textContent = "❌";
    } else {
      span.textContent = "✅";
    }
  }
  

  function validSubmit() {
    if (
      /^[А-я-іІїЇ]+ [А-я-іІїЇ]+$/.test(document.getElementById("names").value) &&
      /^\+380\d\d \d\d\d \d\d \d\d$/.test(document.getElementById("tel").value) &&
      /[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}/i.test(document.getElementById("email").value)
    ) {
      alert("Данні відправлено");
    } else alert("Введіть правильно данні");
  }
});
