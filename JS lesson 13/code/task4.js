// 4.
// - При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
// - Поведінка поля має бути такою:
// - При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
// - Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст:
// .
// Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
// - При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
// - Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою,
// під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
//

const div1 = document.getElementById("close"),
  input = document.getElementById("price"),
  span = document.createElement(`span`),
  spanClose = document.createElement(`button`);

spanClose.innerHTML = "X";

input.addEventListener(
  "input",
  () => (input.value = input.value.replace(/[^0-9\+\-]/g, ""))
);

input.addEventListener("change", valid);
input.addEventListener("focusin", () => {
  span.innerHTML = ``;
  input.style.border = "2px solid green";
  input.style.outline = "0";
  input.style.color = `black`;
});
input.addEventListener("focusout", () => (input.style.border = ""));

spanClose.addEventListener("click", () => {
  div1.innerHTML = ``;
  div1.classList.remove("close");
});

function valid(e) {
  input.classList = ``;
  span.textContent = ``;
  const z = /\d/.test(input.value);

  if (z == true && +input.value >= 0) {
    span.textContent = e.target.value;
    input.style.color = `green`;
    div1.append(span);
    div1.append(spanClose);
  } else {
    input.classList = `red`;
    span.innerHTML = `<br> Please enter correct price`;
    input.after(span);
  }
}
