// 2.Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.

window.addEventListener("DOMContentLoaded", () => {
  const [...inputs] = document.querySelectorAll("input");

  inputs.forEach((element) => {
    element.addEventListener("change", change);
  });

  function change(e) {
    let x = this.getAttribute("data-length");

    if (this.value.length == x) {
      this.classList = "green";
    } else {
      this.classList = "red";
    }
  }
});
