import product from "./product.js";

function card(key, x, index) {
  let i;
  if (index == 1) {
    i = "./";
  } else {
    i = "../";
  }

  const card = `
    <div class="cards">
       <img src="${
         i + product[key].img1
       }" alt="" class="cardimg" data-num="${key}">
      
       <div class="names">${product[key].name}</div>
      
       <div class="rate" id="rate${key}"></div>
       
       <div class="price">
           <p>As low as ${product[key].prise} $</p>
           
       </div>
       <div class="color_card" id="color${key}"></div>
      
       <a href="" id='basket' data-num="${key}" class="basket" onclick="return false"><img src="${i}img/Vector.png" >  ADD TO CART</a>
     
   </div>
    `;

  x.insertAdjacentHTML("beforeend", card);
  //rate
  const rate = document.getElementById("rate" + key);

  for (let k = 1; k <= +product[key].rate; k++) {
    rate.insertAdjacentHTML(
      "afterbegin",
      `<img src="${i}img/star-full.png" alt="star">`
    );
  }
  for (let k = 1; k <= 5 - +product[key].rate; k++) {
    rate.insertAdjacentHTML(
      "beforeend",
      `<img src="${i}img/star.png" alt="star">`
    );
  }
  //color
  const colorCard = document.getElementById("color" + key);

  product[key].color.forEach((color) => {
    switch (color) {
      case "red":
        {
          colorCard.insertAdjacentHTML(
            "beforeend",
            `<img src="${i}img/red.png" alt="green" data-value="red" />`
          );
        }
        break;
      case "green":
        {
          colorCard.insertAdjacentHTML(
            "beforeend",
            `<img src="${i}img/green.png" alt="green" data-value="green" />`
          );
        }
        break;
      case "black":
        {
          colorCard.insertAdjacentHTML(
            "beforeend",
            `<img src="${i}img/black.png" alt="black" data-value="black" />`
          );
        }
        break;
      case "white":
        {
          colorCard.insertAdjacentHTML(
            "beforeend",
            `<img src="${i}img/white.png" alt="white" data-value="white" />`
          );
        }
        break;
      case "blue": {
        colorCard.insertAdjacentHTML(
          "beforeend",
          `<img src="${i}img/blue.png" alt="blue" data-value="blue" />`
        );
      }
    }
  });
}

function bag() {
  document.getElementById("bask").classList.toggle("display_none");
}

function bagDisplay() {
  document.getElementById("bask").classList.add("display_none");
}

function cardsVisual(e, gridCard) {
  let k;
  if (e == 1) {
    k = 1;
  } else {
    k = e * 10;
  }

  for (let x = k; x < k + 10; x++) {
    card(x, gridCard);
  }
}

function productLength(e) {
  return Math.ceil(Object.keys(e).length / 10);
}

function pageNumber(page, divNum) {
  for (let n = 1; n < page; n++) {
    divNum.insertAdjacentHTML(
      "beforeend",
      '<input type="button" class="num_page" value="' + n + '">'
    );
  }
}

function onePage(gridCard) {
  for (let x = 1; x <= 10; x++) {
    card(x, gridCard, 2);
  }
}

function addBag(k) {
  let productBag;
  if (localStorage.getItem("bag")) {
    productBag = JSON.parse(localStorage.getItem("bag"));
  } else {
    localStorage.setItem("bag", JSON.stringify({}));
    productBag = JSON.parse(localStorage.getItem("bag"));
  }

  productBag[k] = {};

  productBag[k]["cost"] = product[k].prise;

  localStorage.setItem("bag", JSON.stringify(productBag));
}

function korzina(k) {
  let cost;

  const arr = JSON.parse(localStorage.getItem("bag"));
  document.getElementById("bag").innerHTML = "";
  if (k) {
    for (let key in arr) {
      let bagCard = `
  <div id='${key}' class="prod">
                
  <img class="bag_img" src="${product[key].img1}">  
  <div>
      <div>${product[key].name}</div>
       <div class="bag_cost" id="cost">COST: ${product[key].prise}</div>
  </div>
   <div class="bag_del" id="bag_del" data-id="${key}">X</div>
  </div>`;

      document.getElementById("bag").insertAdjacentHTML("afterBegin", bagCard);
      if (!cost) {
        cost = 0;
      }
      cost = cost + +arr[key].cost;
    }
  } else {
    for (let key in arr) {
      let bagCard = `
    <div id='${key}' class="prod">
                  
    <img class="bag_img" src=".${product[key].img1}">  
    <div>
        <div>${product[key].name}</div>
         <div class="bag_cost" id="cost">COST: ${product[key].prise}</div>
    </div>
     <div class="bag_del" id="bag_del" data-id="${key}">X</div>
    </div>`;

      document.getElementById("bag").insertAdjacentHTML("afterBegin", bagCard);
      if (!cost) {
        cost = 0;
      }
      cost = cost + +arr[key].cost;
    }
  }

  if (cost == undefined) {
    cost = 0;
  }
  let fullCost = document.getElementById("full_cost");
  fullCost.innerText = `Full COST:  ${cost}`;
}

export {
  card,
  bag,
  addBag,
  bagDisplay,
  cardsVisual,
  productLength,
  pageNumber,
  onePage,
  korzina,
};
