import { card, bagDisplay, bag, addBag, korzina } from "./function.js";

window.addEventListener("DOMContentLoaded", () => {
  // sale cards
  const saleCard = document.getElementById("sale");

  card(12, saleCard, 1);
  card(2, saleCard, 1);
  card(41, saleCard, 1);
  card(6, saleCard, 1);

  //bag
  bagDisplay();

  // відкриття в новій сторінці
  saleCard.addEventListener("click", (e) => {
    const keyDown = e.target.getAttribute("class");
    var key = e.target.getAttribute("data-num");

    if (keyDown == "cardimg") {
      localStorage.setItem("key", key);
      window.open("./product-page/index.html", "_blank");
    }
    if (keyDown == "basket") {
      addBag(key);
    }
  });

  document.getElementById("bag-btn").addEventListener("click", () => {
    bag();
    korzina(1);
  });
  document.getElementById("bask").addEventListener("click", (e) => {
    let id = e.target.getAttribute("data-id");
    if (!id) {
      return;
    }
    let prod = JSON.parse(localStorage.getItem("bag"));
    delete prod[id];
    localStorage.setItem("bag", JSON.stringify(prod));
    korzina(1);
  });
});
