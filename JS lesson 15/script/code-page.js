import product from "../script/product.js";
import {
  card,
  onePage,
  korzina,
  bag,
  addBag,
  bagDisplay,
  cardsVisual,
  productLength,
  pageNumber,
} from "../script/function.js";

window.addEventListener("DOMContentLoaded", () => {
  bagDisplay();

  let arrSize = [];
  let arrColor = [];
  const gridCard = document.getElementById("cards_grid");

  const filterSize = document.getElementById("size");
  const filterColor = document.getElementById("color");

  const divNum = document.getElementById("num");
  const fSize = document.getElementById("filtre_size");
  const fColor = document.getElementById("filtre_color");

  const full = Object.keys(product).length;

  // отрисовка первой страници

  onePage(gridCard);

  pageNumber(productLength(product), divNum);

  filterSize.addEventListener("click", (e) => {
    const dataValue = e.target.getAttribute("data-value");

    if (!dataValue) return;

    fSize.innerText = `Size:  ` + dataValue;
    arrSize = [];
    if (arrColor == 0) {
      // проверка всех карточек
      for (let s = 1; s < full; s++) {
        product[s].size.forEach((element) => {
          // заполнение массива
          if (element == dataValue) {
            arrSize.push(s);
          }
        });
      }
    } else {
      for (let s = 1; s <= arrColor.length; s++) {
        arrColor.forEach((item) => {
          product[item].size.forEach((el) => {
            if (el == dataValue) {
              arrSize.push(item);
            }
          });
        });
      }
    }

    gridCard.innerHTML = "";

    arrSize.forEach((e) => {
      card(e, gridCard);
    });

    divNum.innerHTML = "";
    //pageNumber(productLength(arrSize), divNum);
  });
  //---------------------------
  filterColor.addEventListener("click", (e) => {
    const dataValue = e.target.getAttribute("data-value");

    if (!dataValue) return;

    fColor.innerText = `Color:  ` + dataValue;
    arrColor = [];

    if (arrSize == 0) {
      for (let s = 1; s < full; s++) {
        product[s].color.forEach((element) => {
          // заполнение массива
          if (element == dataValue) {
            arrColor.push(s);
          }
        });
      }
    } else {
      arrSize.forEach((item) => {
        product[item].color.forEach((el) => {
          if (el == dataValue) {
            arrColor.push(item);
          }
        });
      });
    }

    // проверка всех карточек
    gridCard.innerHTML = "";

    arrColor.forEach((e) => {
      card(e, gridCard);
    });
    divNum.innerHTML = "";
    //pageNumber(productLength(arrColor), divNum);
  });

  // отрисовка по страницам по нажатию на страницы
  divNum.addEventListener("click", (e) => {
    // let k;

    const keyDown = e.target.getAttribute("value");
    gridCard.innerHTML = "";
    cardsVisual(keyDown, gridCard);
  });

  // відкриття в новій сторінці
  document.getElementById("cards_grid").addEventListener("click", (e) => {
    const keyDown = e.target.getAttribute("class");
    var key = e.target.getAttribute("data-num");

    if (keyDown == "cardimg") {
      localStorage.setItem("key", key);
      window.open("../product-page/index.html", "_blank");
    }
    //
    if (keyDown == "basket") {
      addBag(key);
    }
  });

  document.getElementById("filtre_cancel").addEventListener("click", () => {
    arrSize = [];
    arrColor = [];
    fSize.innerText = `Size:  `;
    fColor.innerText = `Color:  `;
    gridCard.innerHTML = "";
    onePage(gridCard);

    pageNumber(productLength(product), divNum);
  });

  document.getElementById("bag-btn").addEventListener("click", () => {
    bag();
    korzina();
  });
  document.getElementById("bask").addEventListener("click", (e) => {
    let id = e.target.getAttribute("data-id");
    if (!id) {
      return;
    }
    let prod = JSON.parse(localStorage.getItem("bag"));
    delete prod[id];
    localStorage.setItem("bag", JSON.stringify(prod));
    korzina();
  });
});
