import product from "../script/product.js";
import { bagDisplay, bag, korzina, addBag } from "../script/function.js";

window.addEventListener("DOMContentLoaded", () => {
  const key = localStorage.getItem("key");
  const prod = product[key];

  bagDisplay();

  document.getElementById("spName").innerText = prod.name;

  document.getElementById("img_prod").setAttribute("src", `.` + prod.img1);

  document.getElementById("img_litl1").setAttribute("src", `.` + prod.img1);

  document.getElementById("img_litl2").setAttribute("src", `.` + prod.img2);

  document.getElementById("text_prodf").innerText = prod.name;

  document.getElementById("numID").innerText = key;

  // rate
  for (let k = 1; k <= prod.rate; k++) {
    document
      .getElementById("specifRate")
      .insertAdjacentHTML(
        "afterbegin",
        '<img src="../img/star-full.png" alt="star">'
      );
  }
  for (let k = 1; k <= 5 - prod.rate; k++) {
    document
      .getElementById("specifRate")
      .insertAdjacentHTML(
        "beforeend",
        '<img src="../img/star.png" alt="star">'
      );
  }

  document.getElementById("specifPrice").innerText = "$" + prod.prise + ".00";
  //color
  prod.color.forEach((color) => {
    switch (color) {
      case "red":
        {
          document
            .getElementById("specifSort")
            .insertAdjacentHTML(
              "beforeend",
              '<img class="specif_sort_col" src="../img/red.png" alt="red" data-value="red" />'
            );
        }
        break;
      case "green":
        {
          document
            .getElementById("specifSort")
            .insertAdjacentHTML(
              "beforeend",
              '<img class="specif_sort_col" src="../img/green.png" alt="green" data-value="green" />'
            );
        }
        break;
      case "black":
        {
          document
            .getElementById("specifSort")
            .insertAdjacentHTML(
              "beforeend",
              '<img class="specif_sort_col" src="../img/black.png" alt="black" data-value="black" />'
            );
        }
        break;
      case "white":
        {
          document
            .getElementById("specifSort")
            .insertAdjacentHTML(
              "beforeend",
              '<img class="specif_sort_col" src="../img/white.png" alt="white" data-value="white" />'
            );
        }
        break;
      case "blue": {
        document
          .getElementById("specifSort")
          .insertAdjacentHTML(
            "beforeend",
            '<img class="specif_sort_col" src="../img/blue.png" alt="blue" data-value="blue" /> '
          );
      }
    }
  });
  //size
  prod.size.forEach((size) => {
    document
      .getElementById("sortNum")
      .insertAdjacentHTML(
        "beforeend",
        '<span class="specif_sort_num">' + size + "</span>"
      );
  });

  document.addEventListener("click", (e) => {
    if (e.target.id == "img_litl1") {
      document.getElementById("img_prod").setAttribute("src", `.` + prod.img1);
      document.getElementById("img_litl1").className = "border_red";
      document.getElementById("img_litl2").className = "border_black";
    } else if (e.target.id == "img_litl2") {
      document.getElementById("img_prod").setAttribute("src", `.` + prod.img2);
      document.getElementById("img_litl2").className = "border_red";
      document.getElementById("img_litl1").className = "border_black";
    }

    if (e.target.classList == "specif_sort_num") {
      productSize = e.target.innerText;
    }

    if (e.target.classList == "specif_sort_col") {
      productColor = e.target.dataset.value;
    }
  });

  document.getElementById("bag-btn").addEventListener("click", () => {
    bag();
    korzina();
  });
  document.getElementById("bask").addEventListener("click", (e) => {
    let id = e.target.getAttribute("data-id");
    if (!id) {
      return;
    }
    let prod = JSON.parse(localStorage.getItem("bag"));
    delete prod[id];
    localStorage.setItem("bag", JSON.stringify(prod));
    korzina();
  });

  document.getElementById("add").addEventListener("click", () => {
    addBag(key);
  });
});
