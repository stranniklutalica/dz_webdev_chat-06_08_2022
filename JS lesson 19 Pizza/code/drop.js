// var addSouse = false;
// let addTopping = false;
const souse = document.querySelectorAll(".draggable");
const pizza = document.querySelector(".table");
let topping = {
  sauceClassic: false,
  sauceBBQ: false,
  sauceRikotta: false,
  moc1: false,
  moc2: false,
  moc3: false,
  telya: false,
  vetch1: false,
  vetch2: false,
};
let srcDop, id, idTop;

souse.forEach((dop) => {
  dop.addEventListener("dragstart", function (e) {
    id = e.target.id;

    idTop = topping[id];

    srcDop = this.getAttribute("src");
  });
  dop.addEventListener("dragend", () => {});
});

pizza.addEventListener("dragleave", () => {});
pizza.addEventListener("dragover", function (e) {
  e.preventDefault();
});
pizza.addEventListener("drop", function () {
  const img = document.createElement("img");

  img.setAttribute("src", srcDop);
  
  if (idTop == false) {
    pizza.append(img);
    topping[id] = true;
  }
});

//кнопка сбросу заказу
const cancel = document.getElementById("cancel");
cancel.addEventListener("click", () => {
  topping = {
    sauceClassic: false,
    sauceBBQ: false,
    sauceRikotta: false,
    moc1: false,
    moc2: false,
    moc3: false,
    telya: false,
    vetch1: false,
    vetch2: false,
  };
  pizza.innerHTML = `<img src="Pizza_pictures/klassicheskij-bortik_1556622914630.png" alt="Корж класичний">`;
});

// вывод данных пиццы
function showIngredients(obj){
        
  let price = document.querySelector('.price span')
  let souse = document.querySelector('.sauces span')
  let topings = document.querySelector('.topings span')
  // document.createElement()
  // console.log(obj)
  let priceTotal = parseInt(obj.size.price)
  obj.souse.forEach(e => {
      
      priceTotal += parseInt(e.price)
  })
  // priceTotal = obj.price
  price.innerText = priceTotal
  souse.innerText = obj.price
  topings.innerText = obj.price

}

// let pizzaPrice = {
//   sizing: [{size: 'small', price: '10'},
//       {size: 'mid', price: '20'},
//       {size: 'big', price: '30'}],
//   souse: [{name:'sauceClassic', weight: '40', price: '5'},
//       {name:'sauceBBQ', weight: '40', price: '5'},
//       {name:'sauceRikotta', weight: '40', price: '5'}],
//   topings: [
//       {name: 'cheaze regular', weight: '40', price: '5'},
//       {name: 'Feta', weight: '40', price: '5'},
//       {name: 'Mozzarela', weight: '40', price: '5'},
//       {name: 'Veal', weight: '40', price: '5'},
//       {name: 'Tomatoes', weight: '40', price: '5'},
//       {name: 'Mushrooms', weight: '40', price: '5'}
//   ]
// }