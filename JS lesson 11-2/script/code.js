/*ДЗ - Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ:ММ:СС*/
let s=1
let m=0
let h=0
const sec = document.getElementById("second");
const min = document.getElementById("minute");
const hours = document.getElementById("hours");

const start = document.getElementById("start");
const stopBtn = document.getElementById("stop");
const res = document.getElementById("res");
const backColor = document.getElementById("watch")
var oneStart = 0;

start.onclick = ()=>{
    
    if(oneStart == 0){
        oneStart = 1;
        backColor.style.backgroundColor = "green"
        let timerId = setInterval(()=>{
            if(s<=9){sec.textContent=`0` + s}
            else {
                sec.textContent=s
            }
            if(s==59){
                m++;
                s=0;
                if(m<=9){min.textContent=`0` + m}
            else {
                min.textContent=m
            }
                 }
            if(m==59){
                    h++;
                    m=0;
                    s=0;
                    if(h<=9){hours.textContent=`0` + h}
                else {
                    hours.textContent=h
                }
                     }
        stopBtn.onclick = ()=>{
            backColor.style.backgroundColor = "red"
            oneStart = 0
            
            clearInterval(timerId)
            };
        res.onclick = ()=>{
            backColor.style.backgroundColor = "grey"
            clearInterval(timerId);
            s=0;
            m=0;
            h=0;
            sec.textContent = `00`;
            min.textContent = `00`;
            hours.textContent = `00`;
        }
           s++;
           
        
        }, 1000)
    }else{
        
        return
    }
    
};



